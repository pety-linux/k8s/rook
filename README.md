# Rook-Ceph Intro
Pods in Kubernetes are designed to be ephemeral. So, in order to keep the data, we need to save it on Persistent Volume (PV).

**Ceph**

Ceph is a highly scalable distributed storage solution for block storage, object storage, and shared filesystems. It is software-based, meaning it doesn’t rely on specific hardware features.

One of the ways to dynamically provision PV, is by using rook-ceph.

**CEPH consists of:**

- Monitor (ceph-mon) : maintain maps of the cluster state.
- Managers (ceph-mgr): keeping track of runtime metrics and the current state of the Ceph cluster.
- Object Store Device (ceph-osd): Where data is stored and managed
- Metadata Server (ceph-mds): store metadata

**Rook**

Rook is an operator that simplifies the deployment of Ceph in a Kubernetes cluster.

Rook functions is to:

- Start and monitor Ceph monitor pods, the Ceph OSD daemons to provide RADOS storage, as well as start and manage other Ceph daemons.
- Watch desired state changes requested by the API service and apply the changes.
- Initializes the agents that are needed for consuming the storage.

<br> </br>

# Deploy Rook-Ceph on Kubernetes

### 0. Prerequsities
Ensure that your workers have dedicated raw(unformatted) disks. These disks will become a Ceph OSD.

### 1. Clone the source GitHub code for rook
```
git clone https://github.com/rook/rook.git
cd rook/deploy/examples

kubectl create -f crds.yaml -f common.yaml -f operator.yaml
```

### 2. Verify rook-ceph-operator is in Running state
```
kubectl -n rook-ceph get pod -l app=rook-ceph-operator
```

### 3. Build Ceph cluster via rook
```
kubectl create -f cluster.yaml
```
> NOTE: wait till all pods are Running/Completed

### 4. toolbox for monitoring ceph status 
```
kubectl create -f toolbox.yaml
```

### 5. Check the Ceph status using toolbox 
```
kubectl -n rook-ceph exec deploy/rook-ceph-tools -- ceph status
```

### 6. Create filesystem
```
kubectl -n rook-ceph get pod -l app=rook-ceph-mds
```
> NOTE: There should be two pods in the output, one is the active instance, and the other one is for fail-over.


### 7. Create Ceph StorageClass
```
kubectl -n rook-ceph create -f csi/cephfs/storageclass.yaml
```

### 8. Check
```
k get sc
```

### 9. Ceph admin commands
```
kubectl -n rook-ceph exec deploy/rook-ceph-tools -- ceph status

ceph osd tree
ceph df
```


### Additional Resources
- https://rook.github.io/docs/rook/latest/Getting-Started/quickstart/#tldr
- https://faun.pub/deploy-rook-ceph-on-kubernetes-3a2252f3732e
