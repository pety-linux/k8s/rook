#!/bin/bash
# Deploy rook

### 1. Clone rook repo and apply manifests
git clone https://github.com/rook/rook.git
cd rook/deploy/examples

kubectl create -f crds.yaml -f common.yaml -f operator.yaml

### 2. Verify rook-ceph-operator is in Running state
kubectl -n rook-ceph get pod -l app=rook-ceph-operator

### 3. Build Ceph cluster via rook
kubectl create -f cluster.yaml
sleep 5m

### 4. toolbox for monitoring ceph status 
kubectl create -f toolbox.yaml
sleep 1m

### 5. Check the Ceph status using toolbox 
kubectl -n rook-ceph exec deploy/rook-ceph-tools -- ceph status

### 6. Create filesystem
kubectl create -f filesystem.yaml
sleep 1m

### 7. Check
kubectl -n rook-ceph get pod -l app=rook-ceph-mds

### 8. Create Ceph StorageClass
kubectl -n rook-ceph create -f csi/cephfs/storageclass.yaml
sleep 1m

### 9. Check
kubectl get sc
